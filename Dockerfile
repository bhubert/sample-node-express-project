
# Stage 1: Build the Node app with tsc
FROM node:latest AS builder

WORKDIR /app

# Copy package.json, pnpm-lock.yaml, tsconfig.json, tsconfig.build.json
COPY package.json pnpm-lock.yaml tsconfig.json tsconfig.build.json ./

# Install dependencies with pnpm
RUN npm install -g pnpm
RUN pnpm install --frozen-lockfile

# Copy the rest of the app source code
COPY src/ ./src/

# Build the app with tsc
RUN pnpm build

# Stage 2: Run the app in an image without devDependencies
FROM node:16-alpine

WORKDIR /app

# Copy the built app from the previous stage
COPY --from=builder /app/dist ./dist
COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/pnpm-lock.yaml ./pnpm-lock.yaml

# Install only production dependencies with pnpm
RUN npm install -g pnpm
RUN pnpm install --frozen-lockfile --production

# Expose the app port
EXPOSE 4000

# Start the app
CMD ["pnpm", "start"]
