import express from "express";
import morgan from "morgan";
import cors from "cors";
import dotenv from "dotenv";
import { addition } from "./calculator";

dotenv.config();

const app = express();
app.use(morgan("dev"));
app.use(cors());
app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello World");
});

app.post("/echo", (req, res) => {
  res.json(req.body);
});

app.post("/calculate", (req, res) => {
  const { first, second } = req.body;
  const castFirst = Number(first);
  const castSecond = Number(second);
  if (Number.isNaN(castFirst) || Number.isNaN(castSecond)) {
    return res.status(400).json({ error: "Parameters must be valid numbers." });
  }
  const result = addition(first, second);
  return res.json({ result });
})

export default app;
