// tests/calculator.spec.ts
import assert from "assert";
import { addition } from "../src/calculator";

describe("Calculator Tests", () => {
      it("should return 5 when 2 is added to 3", () => {
      const result = addition(2, 3);
      assert.strictEqual(result, 5);
   });
});