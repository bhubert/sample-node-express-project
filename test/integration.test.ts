import request from "supertest";
import assert from "assert";
import app from "../src/app";

describe("Integration Tests", () => {
  describe("GET /", () => {
    it("should return 200 OK", async () => {
      const result = await request(app).get("/");
      assert.strictEqual(result.status, 200);
      assert.strictEqual(result.text, "Hello World");
    });
  });

  describe("POST /echo", () => {
    it("should return 200 OK with the sent body", async () => {
      const body = { hello: "world" };
      const result = await request(app).post("/echo").send(body);
      assert.strictEqual(result.status, 200);
      assert.deepStrictEqual(result.body, body);
    });
  });

  describe("POST /calculate", () => {
    it("should return 200 OK with the addition result", async () => {
      const body = { first: 2, second: 3 };
      const result = await request(app).post("/calculate").send(body);
      assert.strictEqual(result.status, 200);
      assert.deepStrictEqual(result.body, { result: 5 });
    });

    it("should return 400 Bad Request if the parameters are not numbers", async () => {
      const body = { first: "hello", second: "world" };
      const result = await request(app).post("/calculate").send(body);
      assert.strictEqual(result.status, 400);
      assert.deepStrictEqual(result.body, { error: "Parameters must be valid numbers." });
    });
  });
});